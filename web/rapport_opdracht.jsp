<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ScoreTracker | Rapport per opdracht</title>
    <link href="plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/main-style.css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
   </head>
<body>

        <!--Menu aan de zijkant-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <!--Gebruikersinfo-->
                    <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="img/notepad.png" alt="">
                            </div>
                            <div class="user-info">
                                <div><strong>ScoreTracker</strong></div>
                            </div>
                        </div>
                    </li>
                    <!--Menu-items-->
                    <li class="listItem">
                        <span class="menuItem"><a href="indes.jsp">Overzicht</a></span>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Klassen</span>
                        <ul>
                            <li><a href="upload.jsp">Nieuwe klassenlijst uploaden</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Opdrachten</span>
                        <ul>
                            <li><a href="upload_opdracht.jsp">Nieuwe opdracht uploaden</a></li>
                            <li><a href="KeuzeServlet?resultaten=Y">Resultaten aanpassen</a></li>
                        </ul>
                    </li>
                    <li class="selected listItem">
                        <span class="menuItem">Rapporten</span>
                        <ul>
                            <li><a href="KeuzeServlet?rapportOpdracht=Y">Rapport per opdracht</a></li>
                            <li><a href="KeuzeServlet?rapportStudent=Y">Rapport per student</a></li>
                        </ul>
                    </li> 
                    <li class="listItem">
                        <span class="menuItem">Uitleg</span>
                        <ul>
                            <li><a href="uitleg.jsp">Uitleg</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        
        <!--Groot grijs middenstuk-->
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rapport per opdracht downloaden</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Op deze pagina kan je een test van een klas selecteren en hiervan de resultaten in een pdf downloaden</h3>
                    <c:if test="${keuze == 'klas'}">
                        <form action="KeuzeServlet" method="get">
                        <div>
                            <p>Kies hier de klas:<br/>
                            <select id="klassen"  name="klassen">   
                                <option value="keuze">--Kies klas--</option>
                                <c:forEach var="klas" items="${klassen}">
                                    <option value="${klas.getId()}">${klas.getNaam()}</option>
                                </c:forEach>
                            </select></p>
                            <p><input type="submit" value="Toon testen" name="toonTesten"/></p> 
                        </div>
                        </form>
                    </c:if>
                    
                    <c:if test="${foutKlas != null}">
                        <p class="fout">${foutKlas}</p>
                    </c:if>
                    <c:if test="${foutKlasTest != null}">
                        <p class="fout">${foutKlasTest}</p>
                    </c:if>
                        
                    <form method="GET" action="PdfServlet" enctype="multipart/form-data" >            
                        <c:if test="${keuze == 'test'}">
                        <p>Kies hier de test waarvan je het rapport wilt opvragen:<br/>
                        <select id="testen" name="testen">
                            <option value="keuze">--Kies--</option>
                            <c:forEach var="test" items="${testen}">
                                <option value="${test.getId()}">${test.getNaam()} (${test.getVak().getNaam()})</option>
                            </c:forEach>
                        </select></p>
                        
                        <c:if test="${foutTest != null}">
                            <p class="fout">${foutTest}</p>
                        </c:if>
                            
                        <p><input type="hidden" value="${klasId}" name="klasId"/>
                            <input type="submit" value="Rapport ophalen" name="maakPdf"/></p>
                        
                        <div>
                            <a href="KeuzeServlet?rapportOpdracht=Y">Terug naar klassen</a>
                        </div>
                        </c:if>
                    </form>
                </div>
            </div>
        </div>


    <!-- Core Scripts - Include with every page -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/scripts/dashboard-demo.js"></script>

</body>

</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.classes.Klas;
import be.thomasmore.java.classes.Student;
import be.thomasmore.java.classes.StudentTest;
import be.thomasmore.java.classes.Test;
import be.thomasmore.java.classes.Vak;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author e_for
 */
@WebServlet(name = "PdfServlet", urlPatterns = {"/PdfServlet"})
public class PdfServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;

        //In rapport_opdracht kiezen voor "Rapport ophalen" en een pdf downloaden
        //Foutmelding als er geen test gekozen is
        if (request.getParameter("maakPdf") != null) {
            Long klasId = Long.parseLong(request.getParameter("klasId"));

            if (!request.getParameter("testen").equals("keuze")) {
                Long testId = Long.parseLong(request.getParameter("testen"));

                String url = "http://localhost:8080/ScoreTrackerServices/rest/testen/create/" + klasId + "/" + testId + "?testPdf=TEST";
                request.setAttribute("url", url);
                
                rd = request.getRequestDispatcher("download.jsp");
            } else {
                request.setAttribute("foutTest", "Kies een test!");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/KeuzeServlet?klassen=" + klasId + "&toonTesten=Toon+testen");
                dispatcher.forward(request, response);
            }
        }
        
        //In rapport_student kiezen voor "Rapport ophalen" en een pdf downloaden
        //Foutmelding als er geen student of data gekozen is/zijn
        else if (request.getParameter("rapport") != null)
        {
            Long klasId = Long.parseLong(request.getParameter("klasId"));
            
            if (request.getParameter("gekozenStudent").equals("keuze") || request.getParameter("datepicker").equals("") || request.getParameter("datepicker2").equals(""))
            {
                if (request.getParameter("gekozenStudent").equals("keuze"))
                {
                    request.setAttribute("foutStudent", "Kies een student!");
                }
                if (request.getParameter("datepicker").equals(""))
                {
                    request.setAttribute("foutStartDatum", "Kies een startdatum!");
                }
                if (request.getParameter("datepicker2").equals(""))
                {
                    request.setAttribute("foutEindDatum", "Kies een einddatum!");
                }
                
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/KeuzeServlet?klassen=" + klasId + "&toonStudenten=Toon+studenten");
                dispatcher.forward(request, response);
            }
            else
            {
                Long studentId = Long.parseLong(request.getParameter("gekozenStudent"));
                
                String date1 = request.getParameter("datepicker");
                String[] data1 = date1.split("/");
                String startDatum = data1[0] + "-" + data1[1] + "-" + data1[2];
                String date2 = request.getParameter("datepicker2");
                String[] data2 = date2.split("/");
                String eindDatum = data2[0] + "-" + data2[1] + "-" + data2[2];
                
                int aantalTesten = getAantalTestenPerStudentTussenData(studentId, startDatum, eindDatum, klasId);
                
                if(aantalTesten == 0)
                {
                    request.setAttribute("foutData", "Er bestaan geen opdrachten tussen deze data!<br/>Kies nieuwe data of upload nieuwe opdrachten.");
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/KeuzeServlet?klassen=" + klasId + "&toonStudenten=Toon+studenten");
                    dispatcher.forward(request, response);
                }
                else
                {
                    String url = "http://localhost:8080/ScoreTrackerServices/rest/testen/createDates/" + studentId + "/" + startDatum + "/" + eindDatum + "/"+ "?testPdf=TEST";
                    request.setAttribute("url", url);

                    String url2 = "http://localhost:8080/ScoreTrackerServices/rest/testen/createDatesForStudent/" + studentId + "/" + startDatum + "/" + eindDatum + "/"+ "?testPdf=TEST";
                    request.setAttribute("url2", url2);

                    rd = request.getRequestDispatcher("download.jsp");
                }
            }            
        }

        rd.forward(request, response);
    }

    private int getAantalTestenPerStudentTussenData(Long studentId, String startDatum, String eindDatum, Long klasId) {
        int aantal = 0;
        try {
         
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            //client.register(new ("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget studentTestWebTarget = webTarget.path("entities.studenttest");
            WebTarget AllWebTarget = studentTestWebTarget.path("allPerKlas");
            WebTarget AllPerKlasTarget = AllWebTarget.path(klasId.toString());

            Invocation.Builder invocationBuilder = AllPerKlasTarget.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList2 = doc.getElementsByTagName("studentTest");

            for (int temp = 0; temp < nList2.getLength(); temp++) {
                Node nNode = nList2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    if (Long.parseLong(eElement.getElementsByTagName("id").item(1).getTextContent()) == studentId)
                    {
                        aantal++;
                    }
                }
            }

        } catch (DOMException | NumberFormatException e) {
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PdfServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aantal;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

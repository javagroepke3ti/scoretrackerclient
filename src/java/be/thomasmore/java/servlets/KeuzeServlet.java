/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.classes.Klas;
import be.thomasmore.java.classes.Student;
import be.thomasmore.java.classes.Test;
import be.thomasmore.java.classes.Vak;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author e_for
 */

@WebServlet(name = "KeuzeServlet", urlPatterns = {"/KeuzeServlet"})
public class KeuzeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = null;

        //Als er in overzicht op 'rapport per student' geklikt wordt
        //Alle klassen ophalen en doorsturen naar rapport_student
        if (request.getParameter("rapportStudent") != null) {
            allKlassen(request);
            rd = request.getRequestDispatcher("rapport_student.jsp");
        } 

        //Als er in rapport_student een keuze van klas gemaakt is
        //alle studenten van die klas doorsturen
        //Foutmelding als er geen klas gekozen is
        else if (request.getParameter("toonStudenten") != null) {
            if (!request.getParameter("klassen").equals("keuze")) {
                Long klasId = Long.parseLong(request.getParameter("klassen"));
                List<Test> testen = allTestsFromKlas(klasId, request);
                if (testen.isEmpty()) 
                {
                    allKlassen(request);
                    request.setAttribute("foutKlasTest", "Er zijn nog geen testen voor deze klas!<br/>Geef eerst nieuwe testen in.");
                    rd = request.getRequestDispatcher("rapport_student.jsp");
                }
                else
                {
                    rd = allStudentsFromKlas(klasId, request, rd); 
                }                
            } 
            else {
                allKlassen(request);
                request.setAttribute("foutKlas", "Kies een klas!");
                rd = request.getRequestDispatcher("rapport_student.jsp");
            }
        }
        
        //Als er in overzicht op 'rapport per opdracht' geklikt wordt
        //Alle klassen ophalen en doorsturen naar rapport_opdracht
        else if (request.getParameter("rapportOpdracht") != null) {
            allKlassen(request);
            rd = request.getRequestDispatcher("rapport_opdracht.jsp");
        } 

        //Als er in rapport_opdracht een keuze van klas gemaakt is
        //alle testen van die klas doorsturen
        //Foutmelding als er geen klas gekozen is
        else if (request.getParameter("toonTesten") != null) {
            if (!request.getParameter("klassen").equals("keuze")) 
            {
                Long klasId = Long.parseLong(request.getParameter("klassen"));

                List<Test> testen = allTestsFromKlas(klasId, request);
                if (testen.isEmpty()) 
                {
                    allKlassen(request);
                    request.setAttribute("foutKlasTest", "Er zijn nog geen testen voor deze klas!<br/>Geef eerst nieuwe testen in.");
                } 
                else 
                {
                    request.setAttribute("testen", testen);
                    request.setAttribute("keuze", "test");
                    request.setAttribute("klasId", klasId);
                }
            } 
            else 
            {
                allKlassen(request);
                request.setAttribute("foutKlas", "Kies een klas!");
            }
            
            rd = request.getRequestDispatcher("rapport_opdracht.jsp");
        }
        
        //Als er in overzicht op 'resultaten aanpassen' geklikt wordt
        //Alle klassen ophalen en doorsturen naar resultaten
        else if (request.getParameter("resultaten")!= null)
        {
            allKlassen(request);
            rd = request.getRequestDispatcher("resultaten.jsp");
        }
        
        //Als er in resultaten een keuze van klas gemaakt is
        //alle testen van die klas doorsturen
        //Foutmelding als er geen klas gekozen is
        else if (request.getParameter("toonDeTesten") != null) 
        {
            if (!request.getParameter("klassen").equals("keuze")) 
            {
                Long klasId = Long.parseLong(request.getParameter("klassen"));

                List<Test> testen = allTestsFromKlas(klasId, request);
                if (testen.isEmpty()) 
                {
                    allKlassen(request);
                    request.setAttribute("foutKlasTest", "Er zijn nog geen testen voor deze klas!<br/>Geef eerst nieuwe testen in.");
                } 
                else 
                {
                    request.setAttribute("testen", testen);
                    request.setAttribute("keuze", "test");
                    request.setAttribute("klasId", klasId);
                }
            } 
            else 
            {
                allKlassen(request);
                request.setAttribute("foutKlas", "Kies een klas!");
            }
            
            rd = request.getRequestDispatcher("resultaten.jsp");
        }
        
        rd.forward(request, response);
    }

    private List<Test> allTestsFromKlas(Long klasId, HttpServletRequest request) throws IOException {
        List<Test> testen = new ArrayList<>();
        try {
         
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            //client.register(new ("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget studentTestWebTarget = webTarget.path("entities.studenttest");
            WebTarget AllWebTarget = studentTestWebTarget.path("allPerKlas");
            WebTarget AllPerKlasTarget = AllWebTarget.path(klasId.toString());

            Invocation.Builder invocationBuilder = AllPerKlasTarget.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList2 = doc.getElementsByTagName("studentTest");
            List<String> testIds = new ArrayList<>();

            for (int temp = 0; temp < nList2.getLength(); temp++) {
                Node nNode = nList2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    String testId = eElement.getElementsByTagName("id").item(3).getTextContent();
                    if (!testIds.contains(testId)) {
                        testIds.add(testId);

                        Test test = new Test();
                        test.setId(Long.parseLong(eElement.getElementsByTagName("id").item(3).getTextContent()));
                        test.setNaam(eElement.getElementsByTagName("naam").item(2).getTextContent());

                        Vak vak = new Vak();
                        vak.setId(Long.parseLong(eElement.getElementsByTagName("id").item(4).getTextContent()));
                        vak.setNaam(eElement.getElementsByTagName("naam").item(3).getTextContent());

                        test.setVak(vak);

                        testen.add(test);
                    }
                }
            }

        } catch (DOMException | NumberFormatException e) {
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return testen;
    }

    private void allKlassen(HttpServletRequest request) throws IOException {
        try {
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget klasWebTarget = webTarget.path("entities.klas");
            WebTarget allKlasWebTarget = klasWebTarget.path("all");

            Invocation.Builder invocationBuilder = allKlasWebTarget.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("klas");

            List<Klas> klassen = new ArrayList<>();

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);
                Klas klas = new Klas();

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    klas.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    klas.setNaam(eElement.getElementsByTagName("naam").item(0).getTextContent());
                }

                klassen.add(klas);
            }

            request.setAttribute("klassen", klassen);
            request.setAttribute("keuze", "klas");

        } catch (DOMException | NumberFormatException e) {
        } catch (SAXException | ParserConfigurationException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public RequestDispatcher allStudentsFromKlas(Long klasId, HttpServletRequest request, RequestDispatcher rd) throws IOException {
        try {
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget studentWebTarget = webTarget.path("entities.student");
            WebTarget allPerKlasWebTarget = studentWebTarget.path("findAllPerKlas");
            WebTarget allPerKlasWebTargetWithQueryParam = allPerKlasWebTarget.path(klasId.toString());

            Invocation.Builder invocationBuilder = allPerKlasWebTargetWithQueryParam.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList2 = doc.getElementsByTagName("student");
            List<Student> studenten = new ArrayList<>();

            for (int temp = 0; temp < nList2.getLength(); temp++) {
                Node nNode = nList2.item(temp);
                Student student = new Student();

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    student.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    student.setNaam(eElement.getElementsByTagName("naam").item(1).getTextContent());
                    student.setVoornaam(eElement.getElementsByTagName("voornaam").item(0).getTextContent());
                }

                studenten.add(student);
            }

            request.setAttribute("studenten", studenten);
            request.setAttribute("keuze", "student");
            request.setAttribute("klasId", klasId);
            rd = request.getRequestDispatcher("rapport_student.jsp");
        } catch (DOMException | NumberFormatException e) {
        } catch (SAXException | ParserConfigurationException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rd;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.classes.Klas;
import be.thomasmore.java.classes.Student;
import be.thomasmore.java.classes.StudentTest;
import be.thomasmore.java.classes.Test;
import be.thomasmore.java.classes.Vak;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author e_for
 */
@WebServlet(name = "OpdrachtServlet", urlPatterns = {"/OpdrachtServlet"})
public class OpdrachtServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;     
        
        //Als er in resultaten op 'Resultaten ophalen' geklikt wordt
        //Alle resultaten van de klas voor die test ophalen
        //Met foutmelding indien er geen test geselecteerd is
        if (request.getParameter("haalResultaten") != null) {
            Long klasId = Long.parseLong(request.getParameter("klasId"));

            if (!request.getParameter("testen").equals("keuze")) {
                Long testId = Long.parseLong(request.getParameter("testen"));

                List<StudentTest> testen = allTestsPerKlasPerTest(klasId, testId);

                request.setAttribute("testnaam", testen.get(0).getTest().getNaam());
                request.setAttribute("klasnaam", testen.get(0).getStudent().getKlas().getNaam());
                request.setAttribute("vaknaam", testen.get(0).getTest().getVak().getNaam());
                request.setAttribute("testen", testen);
                request.setAttribute("keuze", "resultaten");
                rd = request.getRequestDispatcher("resultaten.jsp");
            } else {
                request.setAttribute("foutTest", "Kies een test!");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/KeuzeServlet?klassen=" + klasId + "&toonDeTesten=Y");
                dispatcher.forward(request, response);
            }
        } 

        //Als er in resultaten op het potloodje geklikt wordt om
        //de score van een test aan te passen
        else if (request.getParameter("Aanpassen") != null) {
            Long testId = Long.parseLong(request.getParameter("testId"));
            Long klasId = Long.parseLong(request.getParameter("klasId"));
            Long testAanTePassenId = Long.parseLong(request.getParameter("testAanTePassenId"));

            List<StudentTest> testen = allTestsPerKlasPerTest(klasId, testId);

            request.setAttribute("testnaam", testen.get(0).getTest().getNaam());
            request.setAttribute("klasnaam", testen.get(0).getStudent().getKlas().getNaam());
            request.setAttribute("vaknaam", testen.get(0).getTest().getVak().getNaam());
            request.setAttribute("testen", testen);
            request.setAttribute("keuze", "resultaten");
            request.setAttribute("aanTePassen", testAanTePassenId);
            rd = request.getRequestDispatcher("resultaten.jsp");
        } 

        //Als er in resultaten op 'opslaan' geklikt wordt na het aanpassen van de score
        //Ook foutcontrole
        else if (request.getParameter("scoreAanpassen") != null) {
            Long testId = Long.parseLong(request.getParameter("testId"));
            Long klasId = Long.parseLong(request.getParameter("klasId"));
            Long testAanTePassenId = Long.parseLong(request.getParameter("testAanTePassenId"));
            String score = request.getParameter("nieuweScore");
            
            StudentTest aanTePassenTest = updateStudentTest(testAanTePassenId, score, request);

            List<StudentTest> testen = allTestsPerKlasPerTest(klasId, testId);

            request.setAttribute("testnaam", testen.get(0).getTest().getNaam());
            request.setAttribute("klasnaam", testen.get(0).getStudent().getKlas().getNaam());
            request.setAttribute("vaknaam", testen.get(0).getTest().getVak().getNaam());
            request.setAttribute("testen", testen);
            request.setAttribute("keuze", "resultaten");
            request.setAttribute("aangepasteId", aanTePassenTest.getId());
            rd = request.getRequestDispatcher("resultaten.jsp");
        }

        rd.forward(request, response);
    }

    private List<StudentTest> allTestsPerKlasPerTest(Long klasId, Long testId) throws IOException {
        List<StudentTest> testen = new ArrayList<>();
        try {
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget studentTestWebTarget = webTarget.path("entities.studenttest");
            WebTarget AllWebTarget = studentTestWebTarget.path("allPerKlasPerTest");
            WebTarget AllPerKlasTarget = AllWebTarget.path(klasId.toString());
            WebTarget AllPerTestTarget = AllPerKlasTarget.path(testId.toString());

            Invocation.Builder invocationBuilder = AllPerTestTarget.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList2 = doc.getElementsByTagName("studentTest");

            for (int temp = 0; temp < nList2.getLength(); temp++) {
                Node nNode = nList2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    StudentTest test = new StudentTest();

                    test.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    String datum = (eElement.getElementsByTagName("datum").item(0).getTextContent()).substring(0, 10);
                    String[] data = datum.split("-");
                    String nieuweDatum = data[2] + "/" + data[1] + "/" + data[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    Date parsed = null;
                    try {
                        parsed = format.parse(nieuweDatum);
                    } catch (ParseException ex) {
                        Logger.getLogger(OpdrachtServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    java.sql.Date sql = new java.sql.Date(parsed.getTime());
                    test.setDatum(sql);
                    test.setScore(Double.parseDouble(eElement.getElementsByTagName("score").item(0).getTextContent()));

                    Student student = new Student();
                    student.setId(Long.parseLong(eElement.getElementsByTagName("id").item(1).getTextContent()));
                    student.setNaam(eElement.getElementsByTagName("naam").item(1).getTextContent());
                    student.setVoornaam(eElement.getElementsByTagName("voornaam").item(0).getTextContent());
                    student.setStudentnummer(eElement.getElementsByTagName("studentnummer").item(0).getTextContent());

                    Klas klas = new Klas();
                    klas.setId(Long.parseLong(eElement.getElementsByTagName("id").item(2).getTextContent()));
                    klas.setNaam(eElement.getElementsByTagName("naam").item(0).getTextContent());

                    student.setKlas(klas);
                    test.setStudent(student);

                    Test testZelf = new Test();
                    testZelf.setId(Long.parseLong(eElement.getElementsByTagName("id").item(3).getTextContent()));
                    testZelf.setNaam(eElement.getElementsByTagName("naam").item(2).getTextContent());

                    Vak vak = new Vak();
                    vak.setId(Long.parseLong(eElement.getElementsByTagName("id").item(4).getTextContent()));
                    vak.setNaam(eElement.getElementsByTagName("naam").item(3).getTextContent());

                    testZelf.setVak(vak);
                    test.setTest(testZelf);

                    testen.add(test);
                }
            }

        } catch (DOMException | NumberFormatException e) {
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return testen;
    }

    private StudentTest updateStudentTest(Long testAanTePassenId, String score, HttpServletRequest request) throws IOException {
        StudentTest test = new StudentTest();
        try {
            Double nieuweScore = Double.parseDouble(score);
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest").path("entities.studenttest").path("updateTestScore").path(testAanTePassenId.toString()).path(score);
            Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList2 = doc.getElementsByTagName("studentTest");

            for (int temp = 0; temp < nList2.getLength(); temp++) {
                Node nNode = nList2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    test.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    String datum = (eElement.getElementsByTagName("datum").item(0).getTextContent()).substring(0, 10);
                    String[] data = datum.split("-");
                    String nieuweDatum = data[2] + "/" + data[1] + "/" + data[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    Date parsed = null;
                    try {
                        parsed = format.parse(nieuweDatum);
                    } catch (ParseException ex) {
                        Logger.getLogger(OpdrachtServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    java.sql.Date sql = new java.sql.Date(parsed.getTime());
                    test.setDatum(sql);
                    test.setScore(Double.parseDouble(eElement.getElementsByTagName("score").item(0).getTextContent()));

                    Student student = new Student();
                    student.setId(Long.parseLong(eElement.getElementsByTagName("id").item(1).getTextContent()));
                    student.setNaam(eElement.getElementsByTagName("naam").item(1).getTextContent());
                    student.setVoornaam(eElement.getElementsByTagName("voornaam").item(0).getTextContent());
                    student.setStudentnummer(eElement.getElementsByTagName("studentnummer").item(0).getTextContent());

                    Klas klas = new Klas();
                    klas.setId(Long.parseLong(eElement.getElementsByTagName("id").item(2).getTextContent()));
                    klas.setNaam(eElement.getElementsByTagName("naam").item(0).getTextContent());

                    student.setKlas(klas);
                    test.setStudent(student);

                    Test testZelf = new Test();
                    testZelf.setId(Long.parseLong(eElement.getElementsByTagName("id").item(3).getTextContent()));
                    testZelf.setNaam(eElement.getElementsByTagName("naam").item(2).getTextContent());

                    Vak vak = new Vak();
                    vak.setId(Long.parseLong(eElement.getElementsByTagName("id").item(4).getTextContent()));
                    vak.setNaam(eElement.getElementsByTagName("naam").item(3).getTextContent());

                    testZelf.setVak(vak);
                    test.setTest(testZelf);
                }
            }

        } catch (DOMException e) {
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException e) {
                request.setAttribute("foutScore", "Geef een geldige score in!");
            }
        return test;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

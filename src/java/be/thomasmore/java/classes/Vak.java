/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.classes;

import java.util.Collection;

/**
 *
 * @author e_for
 */
public class Vak {
    private Long id;
    private String naam;
    private Collection<Student> studenten;
    private Collection<Test> testen;
    private Collection<VakLeerkracht> vakLeerkrachten;

    public Vak() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Collection<Student> getStudenten() {
        return studenten;
    }

    public void setStudenten(Collection<Student> studenten) {
        this.studenten = studenten;
    }

    public Collection<Test> getTesten() {
        return testen;
    }

    public void setTesten(Collection<Test> testen) {
        this.testen = testen;
    }

    public Collection<VakLeerkracht> getVakLeerkrachten() {
        return vakLeerkrachten;
    }

    public void setVakLeerkrachten(Collection<VakLeerkracht> vakLeerkrachten) {
        this.vakLeerkrachten = vakLeerkrachten;
    }
    
    
}

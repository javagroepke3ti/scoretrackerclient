/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author e_for
 */
@WebServlet(name = "ExcelServlet", urlPatterns = {"/ExcelServlet"})
public class ExcelServlet extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;
        
        //Als er een opdrachtlijst wordt geüpload van een klas die niet bestaat
        if (request.getParameter("klasBestaatNiet") != null)
        {
            String foutKlas = "De klas in de lijst bestaat niet.<br/>Laad eerst een nieuwe klassenlijst op!";
            request.setAttribute("foutKlas", foutKlas);
            
            rd = request.getRequestDispatcher("upload_opdracht_fail.jsp");
        }
        
        //Als er een opdrachtlijst wordt geüpload van een test die reeds bestaat
        else if (request.getParameter("testBestaatAl") != null)
        {
            String foutTest = "Deze test bestaat al!<br/>Je hoeft hem niet nog eens te uploaden...";
            request.setAttribute("foutTest", foutTest);
            
            rd = request.getRequestDispatcher("upload_opdracht_fail.jsp");
        }
        
        //Als er een opdrachtlijst wordt geüpload, en de file is geen Excel
        else if (request.getParameter("geenExcel") != null)
        {
            String foutFile = "Het bestand dat je probeerde te uploaden is geen Excel-bestand!";
            request.setAttribute("foutFile", foutFile);
            
            rd = request.getRequestDispatcher("upload_opdracht_fail.jsp");
        }
        
        //Als er een opdrachtlijst wordt geüpload, en er is een fout in de studentnummer
        else if (request.getParameter("foutStudent") != null)
        {
            String foutStudent = "Niet alle studentnummers zijn gevonden.<br/> Kijk het Excel-bestand na op fouten.";
            request.setAttribute("foutStudent", foutStudent);
            
            rd = request.getRequestDispatcher("upload_opdracht_fail.jsp");
        }
        
        //Als er een klassenlijst wordt geüpload van een klas die al bestaat
        else if (request.getParameter("klasBestaatAl") != null)
        {
            String foutKlas = "De klas in de lijst bestaat al.<br/>Laad een nieuwe klassenlijst op!";
            request.setAttribute("foutKlas", foutKlas);
            
            rd = request.getRequestDispatcher("upload_fail.jsp");
        }
        
        //Als er een opdrachtlijst wordt geüpload, en de file is geen Excel
        else if (request.getParameter("geenExcelFile") != null)
        {
            String foutFile = "Het bestand dat je probeerde te uploaden is geen Excel-bestand!";
            request.setAttribute("foutFile", foutFile);
            
            rd = request.getRequestDispatcher("upload_fail.jsp");
        }
        
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

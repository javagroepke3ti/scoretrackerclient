<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ScoreTracker | Rapport per student</title>
    <link href="plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/main-style.css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script>
    $(function() {
        $( "#datepicker" ).datepicker({dateFormat: 'dd/mm/yy'});
        $( "#datepicker2" ).datepicker({dateFormat: 'dd/mm/yy'});
    });
    </script>
   </head>
<body>

        <!--Menu aan de zijkant-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <!--Gebruikersinfo-->
                    <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="img/notepad.png" alt="">
                            </div>
                            <div class="user-info">
                                <div><strong>ScoreTracker</strong></div>
                            </div>
                        </div>
                    </li>
                    <!--Menu-items-->
                    <li class="listItem">
                        <span class="menuItem"><a href="index.jsp">Overzicht</a></span>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Klassen</span>
                        <ul>
                            <li><a href="upload.jsp">Nieuwe klassenlijst uploaden</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Opdrachten</span>
                        <ul>
                            <li><a href="upload_opdracht.jsp">Nieuwe opdracht uploaden</a></li>
                            <li><a href="KeuzeServlet?resultaten=Y">Resultaten aanpassen</a></li>
                        </ul>
                    </li>
                    <li class="selected listItem">
                        <span class="menuItem">Rapporten</span>
                        <ul>
                            <li><a href="KeuzeServlet?rapportOpdracht=Y">Rapport per opdracht</a></li>
                            <li><a href="KeuzeServlet?rapportStudent=Y">Rapport per student</a></li>
                        </ul>
                    </li> 
                    <li class="listItem">
                        <span class="menuItem">Uitleg</span>
                        <ul>
                            <li><a href="uitleg.jsp">Uitleg</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        
        <!--Groot grijs middenstuk-->
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rapport per student downloaden</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Opvragen van het rapport van een bepaalde student in een bepaalde tijdspanne:</h3>
                    <c:if test="${keuze == 'klas'}">
                        <form action="KeuzeServlet" method="get">
                        <div>
                            <p>Kies hier de klas:<br/>
                            <select id="klassen"  name="klassen">   
                                <option value="keuze">--Kies klas--</option>
                                <c:forEach var="klas" items="${klassen}">
                                    <option value="${klas.getId()}">${klas.getNaam()}</option>
                                </c:forEach>
                            </select></p>
                            <p><input type="submit" value="Toon studenten" name="toonStudenten"/></p> 
                        </div>
                        </form>
                    </c:if>
                        
                    <c:if test="${foutKlas != null}">
                        <p class="fout">${foutKlas}</p>
                    </c:if>
                    
                    <c:if test="${foutKlasTest != null}">
                        <p class="fout">${foutKlasTest}</p>
                    </c:if>
                            
                    <form action="PdfServlet" method="get">
                        <c:if test="${keuze == 'student'}">
                        <div>
                            <p>Kies hier de student waarvan je het rapport wilt opvragen:<br/>
                            <select name="gekozenStudent">
                                <option value="keuze">--Kies--</option>
                                <c:forEach var="student" items="${studenten}">
                                    <option value="${student.getId()}">${student.getNaam()} ${student.getVoornaam()}</option>
                                </c:forEach>
                            </select></p>
                            
                            <c:if test="${foutStudent != null}">
                                <p class="fout">${foutStudent}</p>
                            </c:if>

                            <p>Kies hier de begindatum:<br/>
                            <input type="text" id="datepicker" name="datepicker"/></p>
                            
                            <c:if test="${foutStartDatum != null}">
                                <p class="fout">${foutStartDatum}</p>
                            </c:if>

                            <p>En de einddatum:<br/>
                            <input type="text" id="datepicker2" name="datepicker2"/></p>
                            
                            <c:if test="${foutEindDatum != null}">
                                <p class="fout">${foutEindDatum}</p>
                            </c:if>

                            <p><input type="hidden" value="${klasId}" name="klasId"/>
                               <input type="submit" value="Rapport ophalen" name="rapport"/></p>  
                            
                            <c:if test="${foutData != null}">
                                <p class="fout">${foutData}</p>
                            </c:if>
                        </div>
                              
                        <div>
                            <a href="KeuzeServlet?rapportStudent=Y">Terug naar klassen</a>
                        </div>
                        </c:if>
                        
                    </form>
                </div>
            </div>
        </div>


    <!-- Core Scripts - Include with every page -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/scripts/dashboard-demo.js"></script>

</body>

</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.classes;

import java.util.Collection;

/**
 *
 * @author e_for
 */
public class Leerkracht {
    private Long id;
    private String gebruikersnaam;
    private String naam;
    private String voornaam;
    private String wachtwoord;
    private Collection<VakLeerkracht> vakLeerkrachtCollection;

    public Leerkracht() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGebruikersnaam() {
        return gebruikersnaam;
    }

    public void setGebruikersnaam(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public Collection<VakLeerkracht> getVakLeerkrachtCollection() {
        return vakLeerkrachtCollection;
    }

    public void setVakLeerkrachtCollection(Collection<VakLeerkracht> vakLeerkrachtCollection) {
        this.vakLeerkrachtCollection = vakLeerkrachtCollection;
    }
    
    
}

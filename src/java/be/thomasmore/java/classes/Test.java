/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.classes;

import java.util.Collection;

/**
 *
 * @author e_for
 */
public class Test {
    private Long id;
    private String naam;
    private Collection<Klas> klassen;
    private Vak vak;
    private Collection<StudentTest> studenten;

    public Test() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Collection<Klas> getKlassen() {
        return klassen;
    }

    public void setKlassen(Collection<Klas> klassen) {
        this.klassen = klassen;
    }

    public Vak getVak() {
        return vak;
    }

    public void setVak(Vak vak) {
        this.vak = vak;
    }

    public Collection<StudentTest> getStudenten() {
        return studenten;
    }

    public void setStudenten(Collection<StudentTest> studenten) {
        this.studenten = studenten;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.classes.Klas;
import be.thomasmore.java.classes.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author e_for
 */
@WebServlet(name = "MailServlet", urlPatterns = {"/MailServlet"})
public class MailServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;
        
        if (request.getParameter("student") != null)
        {
            Long studentId = Long.parseLong(request.getParameter("student"));
            
            Student student = getStudent(studentId);
            
            request.setAttribute("studentNaam", student.getNaam() + " " + student.getVoornaam());
            rd = request.getRequestDispatcher("email_succes.jsp");
            
        }
        
        rd.forward(request, response);
    }

    private Student getStudent(Long studentId) throws IOException {
        Student student = new Student();
        try {
            Client client = ClientBuilder.newClient().register(new AuthenticatorClass("user","user"));
            WebTarget webTarget = client.target("http://localhost:8080/ScoreTrackerServices/rest");
            WebTarget studentWebTarget = webTarget.path("entities.student");
            WebTarget eenStudentWebTarget = studentWebTarget.path(studentId.toString());

            Invocation.Builder invocationBuilder = eenStudentWebTarget.request(MediaType.APPLICATION_XML);

            Response response2 = invocationBuilder.get();
            String xml = response2.readEntity(String.class);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("student");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    student.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    student.setNaam(eElement.getElementsByTagName("naam").item(1).getTextContent());
                    student.setVoornaam(eElement.getElementsByTagName("voornaam").item(0).getTextContent());
                    student.setEmail(eElement.getElementsByTagName("email").item(0).getTextContent());
                    student.setStudentnummer(eElement.getElementsByTagName("studentnummer").item(0).getTextContent());
                }
            }

        } catch (DOMException | NumberFormatException e) {
        } catch (SAXException | ParserConfigurationException ex) {
            Logger.getLogger(KeuzeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return student;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ScoreTracker | Uitleg</title>
        <link href="plugins/bootstrap/bootstrap.css" rel="stylesheet" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/main-style.css" rel="stylesheet" />
        <!-- Page-Level CSS -->
        <link href="plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
    </head>
    <body>

        <!--Menu aan de zijkant-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <!--Gebruikersinfo-->
                    <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="img/notepad.png" alt="">
                            </div>
                            <div class="user-info">
                                <div><strong>ScoreTracker</strong></div>
                            </div>
                        </div>
                    </li>
                    <!--Menu-items-->
                    <li class="listItem">
                        <span class="menuItem"><a href="index.jsp">Overzicht</a></span>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Klassen</span>
                        <ul>
                            <li><a href="upload.jsp">Nieuwe klassenlijst uploaden</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Opdrachten</span>
                        <ul>
                            <li><a href="upload_opdracht.jsp">Nieuwe opdracht uploaden</a></li>                            
                            <li><a href="KeuzeServlet?resultaten=Y">Resultaten aanpassen</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Rapporten</span>
                        <ul>
                            <li><a href="KeuzeServlet?rapportOpdracht=Y">Rapport per opdracht</a></li>
                            <li><a href="KeuzeServlet?rapportStudent=Y">Rapport per student</a></li>
                        </ul>
                    </li> 
                    <li class="selected listItem">
                        <span class="menuItem">Uitleg</span>
                        <ul>
                            <li><a href="uitleg.jsp">Uitleg</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <!--Groot grijs middenstuk-->
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Uitleg</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Uitleg over ScoreTracker</h3>
                    <p>Beste mevrouw Mangelschots,<br/>
                        Beste mijnheer De Schutter,</p>
                    <p>Hartelijk welkom op het ScoreTracker-project van team 1!</p>
                    <p>Alvorens u ons project begint te testen, raden wij u aan de OpVulServlet uit te voeren,
                    die u kan vinden in het project ScoreTrackerServices. Zo heeft u meteen wat testdata ter
                    beschikking!</p>
                    <hr/>
                    <p>U vindt de vier gevraagde webservices als volgt terug:
                    <ul>
                        <li><h4>Klassenlijst uploaden</h4>
                        Klik in het menu op "Nieuwe klassenlijst uploaden". Als u rekening houdt met de gevraagde opstelling
                        van het Excel-document, kan u zonder problemen nieuwe klassen en hun studenten uploaden in de database.</li>
                        <li><h4>Resultaten van één opdracht uploaden</h4>
                        Klik in het menu op "Nieuwe opdracht uploaden". Als u rekening houdt met de gevraagde opstelling
                        van het Excel-document, kan u zonder problemen opdrachten of testen uploaden in de database.</li>
                        <li><h4>Rapport downloaden</h4>
                        Klik in het menu op "Rapport per opdracht". U dient eerst een klas te kiezen, en daarna de test waarvan
                        u een pdf wilt downloaden. Wanneer u uiteindelijk op de "Download"-knop klikt, krijgt u een mooie (al zeggen
                        we het zelf) pdf ter beschikking.</li>
                        <li><h4>Periodiek rapport downloaden</h4>
                        Klik in het menu op "Rapport per student". U dient eerst een klas te kiezen, en daarna de student waarvan
                        u een pdf wilt downloaden. Na het ingeven van de student en de data kan u er enerzijds voor kiezen om het
                        rapport te downloaden, of (voor extra punten!) het rapport rechtstreeks naar de student te sturen.<br/>
                        We hebben de studenten in de testdata allemaal hetzelfde emailadres gegeven, zodat u deze laatste service
                        ook uitgebreid kan testen:
                        <ul>
                            <li>Email: testscoretracker@gmail.com</li>
                            <li>Wachtwoord: scoretracker</li>
                        </ul></li>                        
                    </ul></p>
                    <hr/>
                    <p>Als extra functionaliteiten hebben we het volgende uitgewerkt:
                    <ul>
                        <li><h4>Lijsten opvragen</h4>
                        Deze functionaliteit vindt u terug in het opvragen van de pdf-documenten (kijk maar naar
                        de dropdown-menu's, alsook in de functionaliteit 'Resultaten aanpassen'</li>
                        <li><h4>Resultaten aanpassen</h4>
                        Klik in het menu op "Resultaten aanpassen". U dient eerst een klas te kiezen, en daarna de test waarvan u
                        de resultaten wilt aanpassen. U kan dan per student die de test aflegde, het resultaat aanpassen. Hiervoor
                        maakten we ook gebruik van lijsten (zie vorig puntje).</li>
                        <li><h4>Rapport versturen naar student</h4>
                        Klik in het menu op "Rapport per student". U dient eerst een klas te kiezen, en daarna de student waarvan
                        u de pdf wilt versturen. Na het ingeven van de student en de data kan u ervoor kiezen om het rapport 
                        rechtstreeks naar de student te sturen.</li>
                        <li><h4>Alleen geregistreerde gebruikers</h4>
                        Bij het ophalen van de services voor het uploaden van Excel-bestanden en het downloaden van Pdf-bestanden, 
                        dient uzelf te authenticeren met gebruikersnaam 'user' en wachtwoord 'user'.</li>
                    </ul></p>
                    <hr/>
                    <p>Zoals u duidelijk kan zien, hebben we ons met hart en ziel ingezet voor dit project. 
                        En hoewel het ons bloed, zweet en tranen heeft gekost, zijn we trots op het resultaat!<br/>
                        We hopen dan ook dat u er even hard van zult genieten als wij!</p>
                    <br/>
                    <p>Met vriendelijke groeten,<br/>
                    Team 1:
                    <ul>
                        <li>Thomas Van Rhee</li>
                        <li>Eef Janssens</li>
                        <li>Roy Jansens</li>
                        <li>Jonathan Krols</li>
                    </ul></p>
                </div>
            </div>
        </div>


        <!-- Core Scripts - Include with every page -->
        <script src="assets/plugins/jquery-1.10.2.js"></script>
        <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="assets/plugins/pace/pace.js"></script>
        <script src="assets/scripts/siminta.js"></script>
        <!-- Page-Level Plugin Scripts-->
        <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
        <script src="assets/plugins/morris/morris.js"></script>
        <script src="assets/scripts/dashboard-demo.js"></script>

    </body>

</html>

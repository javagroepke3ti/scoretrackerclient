/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.classes;

import java.util.Collection;

/**
 *
 * @author e_for
 */
public class Student {
    private Long id;
    private String email;
    private String naam;
    private String studentnummer;
    private String voornaam;
    private Collection<Vak> vakken;
    private Klas klas;
    private Collection<StudentTest> studentTesten;

    public Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getStudentnummer() {
        return studentnummer;
    }

    public void setStudentnummer(String studentnummer) {
        this.studentnummer = studentnummer;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public Collection<Vak> getVakken() {
        return vakken;
    }

    public void setVakken(Collection<Vak> vakken) {
        this.vakken = vakken;
    }

    public Klas getKlas() {
        return klas;
    }

    public void setKlas(Klas klas) {
        this.klas = klas;
    }

    public Collection<StudentTest> getStudentTesten() {
        return studentTesten;
    }

    public void setStudentTesten(Collection<StudentTest> studentTesten) {
        this.studentTesten = studentTesten;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.classes;

import java.util.Collection;

/**
 *
 * @author e_for
 */
public class VakLeerkracht {
    private Long id;
    private Collection<Klas> klassen;
    private Leerkracht leerkracht;
    private Vak vak;

    public VakLeerkracht() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Klas> getKlassen() {
        return klassen;
    }

    public void setKlassen(Collection<Klas> klassen) {
        this.klassen = klassen;
    }

    public Leerkracht getLeerkracht() {
        return leerkracht;
    }

    public void setLeerkracht(Leerkracht leerkracht) {
        this.leerkracht = leerkracht;
    }

    public Vak getVak() {
        return vak;
    }

    public void setVak(Vak vak) {
        this.vak = vak;
    }
    
    
}
